/*
  The MIT License

  Copyright © 2018 James C. De Ricco

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

const cm = CodeMirror(document.querySelector('#cm'));
cm.setValue(map.toString())
const runButton = document.querySelector('#run');
const canvasOriginal = document.querySelector('#canvasOriginal');
const ctxOriginal = canvasOriginal.getContext('2d');
const ctxModified = document.querySelector('#canvasModified').getContext('2d');

canvasOriginal.ondrop = function (event) {
  event.preventDefault();

  const dt = event.dataTransfer;
  if (dt.items && dt.items.length > 0 && dt.items[0].kind === 'file' && dt.items[0].type.startsWith('image/')) {
    const file = dt.items[0].getAsFile();
    if (file) {
      img = new Image();
      img.onload = function () {
	URL.revokeObjectURL(file);
	updateOnImgLoad.bind(this)();
      };
      img.src = URL.createObjectURL(file);
    }
  }
};

canvasOriginal.ondragover = function (event) {
  event.preventDefault();
};

runButton.addEventListener('click', updateModifiedImage);

function updateOnImgLoad() {
  ctxOriginal.canvas.width = this.width;
  ctxOriginal.canvas.height = this.height;
  ctxOriginal.drawImage(this, 0, 0);

  ctxModified.canvas.width = this.width;
  ctxModified.canvas.height = this.height;
  
  updateModifiedImage();
}

function updateModifiedImage() {
  eval(cm.getValue());
  const originalImageData = ctxOriginal.getImageData(0, 0, ctxOriginal.canvas.width, ctxOriginal.canvas.height);
  const newImageData = applyTransformer(originalImageData, map);
  ctxModified.putImageData(newImageData, 0, 0);
}

function cloneImageData(imageData) {
  const newData = new Uint8ClampedArray(imageData.data);
  const newImageData = new ImageData(newData, imageData.width, imageData.height);
  
  return newImageData;
}

function applyTransformer(imageData, transformer) {
  const newImageData = cloneImageData(imageData);
  const oldData = imageData.data;
  const newData = newImageData.data;
  const width = imageData.width;
  const height = imageData.height;
  
  for (let i = 0; i < oldData.length; i += 4) {
    const r = oldData[i];
    const g = oldData[i + 1];
    const b = oldData[i + 2];
    const index = Math.floor(i / 4);
    const y = Math.floor(index / width);
    const x = index - y * width;
    const newPixel = transformer(r, g, b, x, y);
    newData[i] = newPixel[0];
    newData[i + 1] = newPixel[1];
    newData[i + 2] = newPixel[2];
  }
  
  return newImageData;
}

function map(r, g, b, x, y) {
  return [r, g, b];
}
